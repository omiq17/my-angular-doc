# My Angular Doc #

Basic things to know about Angular. I have made this repo as a learning material for Angular JS.

### What is this repository for? ###

* Angular JS
* Version 2

### How do I get the doc? ###

* Just read this [Wiki](https://bitbucket.org/omiq17/my-angular-doc/wiki/Home) file.

### Contribution guidelines ###

* You are cordially invited to work here.
* Just give pull request with your appropriate changes.

### Who do I talk to? ###

* Repo owner